import {sum,multiply} from "../src/calc";
import { expect } from "chai";

describe("The calc module", () => {
    context(`#sum`, ()=> {
        it(`should exist`, () => {
            expect(sum).to.be.a("function")
            expect(sum).to.be.instanceOf(Function)
        })
        it(`should sum two numbers`, () => {
            let actual = sum(2,3);
            expect(actual).to.equal(5);
        })
        it(`should sum severals numbers`, () => {
            let actual = sum(2,3,4,5);
            expect(actual).to.equal(14);
        })
    })
    context(`#multiply`, ()=> {
        it(`should exist`, () => {
            expect(multiply).to.be.a("function");
            expect(multiply).to.be.instanceOf(Function)
        })
        it(`should multiply two numbers`, () => {
            let actual = multiply(2,3);
            expect(actual).to.eql([6]);
        })
        it(`should multiply severals numbers`, () => {
            let actual = multiply(1,2,3,4);
            expect(actual).to.eql([2,3,4]);
        })
    })
    context(`#async tests`, ()=> {
        const delay= (ms:number)=> new Promise((resolve)=>setTimeout(resolve,ms));
       
        it(`should multiply two numbers with delay`, () => {
            let actual = multiply(2,3,4,5,6);
            expect(actual).to.eql([6,8,10,12]);
        })
        it(`should multiply severals numbers with delay`, async() => {
            await delay(1000);
            let actual = multiply(2,4,5,6);
            expect(actual).to.eql([8,10,12]);
        })
    })
})