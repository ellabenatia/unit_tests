//sum function that takes any number of numbers and returns their sum number. 
export function sum(...nums:number[]):number{
 return nums.reduce((item,prev)=>(item+prev));
}
export function multiply(multiplier:number,...nums:number[]):number[]{
    return nums.map(item=>(item*multiplier));
   }